package fr.soleil.tango.server.statecomposer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceManagement;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.StateMachine;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.command.ICommandBehavior;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;
import org.tango.server.dynamic.command.GroupCommand;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.StateUtilities;
import fr.esrf.TangoApi.Group.Group;
import fr.soleil.tango.statecomposer.StateResolver;

/**
 * Class Description: This device provides a resum state from a given list of devices state.This device can also execute
 * a grouped command on several device
 *
 * @author FOURNEAU
 *
 */

@Device(transactionType = TransactionType.NONE)
public final class StateComposer {

    private static final int CTE_3000 = 3000;
    /**
     * SimpleDateFormat to timeStamp the messages
     */
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    private Logger logger;
    private final XLogger xlogger = XLoggerFactory.getXLogger(StateComposer.class);

    /**
     * MAIN
     */
    public static void main(final String[] args) {
        final ResourceBundle rb = ResourceBundle.getBundle("fr.soleil.statecomposer.application");
        version = rb.getString("project.version");
        ServerManager.getInstance().start(args, StateComposer.class);
    }

    /**
     * The request grouped commands
     */
    @DeviceProperty
    private String[] commandNameList;

    /**
     * The list of device used to composed the resum state.
     */
    @DeviceProperty
    private String[] deviceNameList;

    /**
     * The time out for a device proxy
     */
    @DeviceProperty
    private int individualTimeout = CTE_3000;

    /**
     * The internal period of the Reading Thread
     */
    @DeviceProperty
    private long internalReadingPeriod = CTE_3000;
    /**
     * The priority value for a state : STATE,priority
     */
    @DeviceProperty
    private String[] priorityList;

    /**
     * The last state evet : STATE at DATE
     */
    @Attribute
    private String lastStateEvent = "";

    /**
     * The number version of the device
     */
    @Attribute(displayLevel = DispLevel._EXPERT)
    private static String version = "";

    /**
     * The state of the device
     */
    @State
    private volatile DeviceState state = DeviceState.UNKNOWN;

    private StateResolver stateReader;

    /**
     * The status of the device
     */
    @Status
    private volatile String status = "";

    @DynamicManagement
    private DynamicManager dynMngt;

    @DeviceManagement
    private DeviceManager device;

    private final List<String> fullDeviceNameList = new ArrayList<String>();

    /**
     * circuit breaker pattern for initializing the device
     *
     * @author ABEILLE
     *
     */
    private class InitCommand {

        private static final int RETRIES_BEFORE_OPEN_CIRCUIT_BREAKER = 3;
        private static final int WAIT_CIRCUIT_BREAKER_OPEN = 30000;
        private static final int WAIT_CIRCUIT_BREAKER_CLOSE = 3000;
        private int retryNr = 0;

        public void execute(final Set<String> deviceNameSet) throws DevFailed {

            // Create the group
            final Group tangoGroup = new Group("statecomposer");
            tangoGroup.add(deviceNameSet.toArray(new String[deviceNameSet.size()]));

            try {
                tangoGroup.set_timeout_millis(individualTimeout, true);
            } catch (final DevFailed e) {
                // not important, could failed if some devices are down
            }
            if (commandNameList.length > 0 && !commandNameList[0].trim().equals("")) {
                // use set to suppress duplicate elements
                final Set<String> cmdList = new HashSet<String>(Arrays.asList(commandNameList));
                for (final String element : cmdList) {
                    final String commandName = element.trim();
                    if (!commandName.isEmpty()) {
                        final GroupCommand behavior = new GroupCommand(commandName, tangoGroup);
                        dynMngt.addCommand(behavior);
                    }
                }
            }
            createStateRefresher(deviceNameSet);
        }

        public void getFallback() {
            retryNr++;
            try {
                logger.info("init failure fallback");
                partialDelete();
                dynMngt.clearAttributesWithExclude("log");
            } catch (final DevFailed e1) {
                // ignore
            }
            // wait before retrying
            if (retryNr == RETRIES_BEFORE_OPEN_CIRCUIT_BREAKER) {
                retryNr = 0;
                try {
                    Thread.sleep(WAIT_CIRCUIT_BREAKER_OPEN);
                } catch (final InterruptedException e1) {
                    Thread.currentThread().interrupt();
                }
            } else {
                try {
                    Thread.sleep(WAIT_CIRCUIT_BREAKER_CLOSE);
                } catch (final InterruptedException e1) {
                    Thread.currentThread().interrupt();
                }
            }

        }
    }

    /**
     * Initialize the device.
     */
    @Init(lazyLoading = true)
    @StateMachine(endState = DeviceState.UNKNOWN)
    public void initDevice() throws DevFailed {
        xlogger.entry();
        logger = LoggerFactory.getLogger(StateComposer.class.getSimpleName() + "." + device.getName());
        dynMngt.addAttribute(new org.tango.server.attribute.log.LogAttribute(1000, logger));
        if (deviceNameList.length > 0 && !deviceNameList[0].equals("")) {
            // remove the double entries thanks to a Set
            final Set<String> deviceNameSet = new LinkedHashSet<String>();
            for (final String deviceName : deviceNameList) {
                if (deviceName.contains("*")){
                    final List<String> deviceNames = Arrays.asList(org.tango.utils.TangoUtil
                            .getDevicesForPattern(deviceName));
                    deviceNameSet.addAll(deviceNames);
                    for (final String device : deviceNames){
                        new DeviceProxy(device);
                    }
                }
                else{
                    deviceNameSet.add(deviceName.trim());
                    // check if device exists because add will not check it
                    new DeviceProxy(deviceName);
                }
            }
            boolean ok = false;
            final InitCommand cmd = new InitCommand();
            while (!ok) {
                try {
                    logger.info("trying init");
                    status = "trying to init";
                    cmd.execute(deviceNameSet);
                    ok = true;
                } catch (final DevFailed e) {
                    logger.error("init failed: {}", DevFailedUtils.toString(e));
                    ok = false;
                    status = "INIT FAILED, will retry in a while\n";
                    status = status + DevFailedUtils.toString(e);
                    cmd.getFallback();
                }
            }
        } else {
            DevFailedUtils.throwDevFailed("INIT_ERROR", "property DeviceNameList is not configured");
        }
        logger.info("init finished");
        xlogger.exit();
    }

    private void createStateRefresher(final Set<String> deviceNameSet) throws DevFailed {
        // create state refresher

        if (deviceNameSet.size() > 0) {
            // retrieve full name from pattern
            for (final String devicePattern : deviceNameSet) {
                if (devicePattern.contains("*")) {
                    final List<String> deviceNames = Arrays.asList(org.tango.utils.TangoUtil
                            .getDevicesForPattern(devicePattern));
                    fullDeviceNameList.addAll(deviceNames);
                } else {
                    fullDeviceNameList.add(devicePattern);
                }
            }
            stateReader = new StateResolver(internalReadingPeriod, false);
            stateReader.configurePriorities(priorityList);
            stateReader.setMonitoredDevices(individualTimeout,
                    fullDeviceNameList.toArray(new String[fullDeviceNameList.size()]));
            stateReader.start(device.getName());
        } else {
            DevFailedUtils.throwDevFailed("INIT_ERROR", "property DeviceNameList is not configured");
        }

    }

    /**
     * Clear all
     *
     * @throws DevFailed
     */

    @Delete
    public void deleteDevice() throws DevFailed {
        partialDelete();
        dynMngt.clearAll();
    }

    /**
     * clear to retry init
     *
     * @throws DevFailed
     */
    private void partialDelete() throws DevFailed {
        fullDeviceNameList.clear();
        if (stateReader != null) {
            stateReader.stop();
        }
    }

    /**
     * The results of the command
     */
    @Attribute(name = "commandsResultReport")
    public String[] getCommandsResultReport() {
        xlogger.entry();
        final Map<String, String> commandReportMap = new HashMap<String, String>();
        final List<ICommandBehavior> commands = dynMngt.getDynamicCommands();
        for (final ICommandBehavior command : commands) {
            if (command instanceof GroupCommand) {
                commandReportMap.putAll(((GroupCommand) command).getErrorReportMap());
            }
        }
        commandReportMap.putAll(stateReader.getErrorReportMap());
        final String[] commandsResultReport = new String[commandReportMap.size()];
        int tmpIndex = 0;
        for (final Map.Entry<String, String> entry : commandReportMap.entrySet()) {
            commandsResultReport[tmpIndex] = "--------\n" + entry.getKey() + " : " + entry.getValue();
            tmpIndex++;
        }
        xlogger.exit();
        return commandsResultReport;
    }

    /**
     * Execute command "GetDeviceForIndex" on device. This command the device name for a given index of the spectrum
     * attributes (devicesStateList or devicesNumberStateList)
     *
     * @param argin
     *            : The index of the spectrum attribute
     * @return The name of the associated device
     */
    @Command(name = "GetDeviceForIndex")
    public String getDeviceForIndex(final short argin) throws DevFailed {
        xlogger.entry();
        String argout = "NULL";

        if (argin < fullDeviceNameList.size() && argin > -1) {
            argout = fullDeviceNameList.get(argin);
        }
        xlogger.exit();
        return argout;
    }

    /**
     * The list of the devices state in number format. Call GetDeviceForIndex to know which device corresponds to an
     * index of the spectrum. Call GetTangoStates to know the values of tango states.
     */
    @Attribute(name = "devicesNumberStateList")
    public short[] getDevicesNumberStateList() {
        xlogger.entry();
        xlogger.exit();
        return stateReader.getDeviceStateNumberArray();
    }

    /**
     * The list of the devices state in string format. Call GetDeviceForIndex to know which device corresponds to an
     * index of the spectrum
     */
    @Attribute(name = "devicesStateList")
    public String[] getDevicesStateList() {
        xlogger.entry();
        xlogger.exit();
        return stateReader.getDeviceStateArray();
    }

    /**
     * Execute command "GetPriorityForState" on device. This command return the priority associated to a given State.
     *
     * @param argin
     *            The State name (ex:ON, OFF, FAULT)
     * @return The priority of the state
     */
    @Command(displayLevel = 1, name = "GetPriorityForState")
    public short getPriorityForState(final String argin) throws DevFailed {
        xlogger.entry();
        short argout = (short) 0;
        if (StateUtilities.isStateExist(argin)) {
            final DevState devStatetmp = StateUtilities.getStateForName(argin);
            argout = (short) stateReader.getPriorityForState(devStatetmp);
        }
        xlogger.exit();
        return argout;
    }

    public DeviceState getState() {
        if (stateReader != null && stateReader.isStarted()) {
            final DeviceState newState = DeviceState.getDeviceState(stateReader.getState());
            if (!state.equals(newState)) {
                lastStateEvent = newState.toString() + " at " + dateFormat.format(new Date());
                state = newState;
            }
        } else {
            state = DeviceState.UNKNOWN;
        }
        return state;
    }

    public String getStatus() {
        if (stateReader != null && stateReader.isStarted()) {
            status = "At least one device is " + getState();
        }
        // else {
        // status = "Composed state yet not available";
        // }
        return status;
    }

    /**
     * Execute command "GetTangoStates" on device. This command return the list of the TANGO states and their associated
     * values. Ex : ON = 0, OFF=1
     *
     * @return The list of the state with the associated number
     */
    @Command(displayLevel = 1, name = "GetTangoStates")
    public String[] getTangoStates() throws DevFailed {
        xlogger.entry();
        xlogger.exit();
        return StateUtilities.STATELIST;
    }

    public String getLastStateEvent() {
        return lastStateEvent;
    }

    public String getVersion() {
        return version;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public void setState(final DeviceState state) {
        this.state = state;
    }

    public void setDynMngt(final DynamicManager dynMngt) {
        this.dynMngt = dynMngt;
    }

    public void setCommandNameList(final String[] commandNameList) {
        this.commandNameList = Arrays.copyOf(commandNameList, commandNameList.length);
    }

    public void setDeviceNameList(final String[] deviceNameList) {
        this.deviceNameList = Arrays.copyOf(deviceNameList, deviceNameList.length);
    }

    public void setPriorityList(final String[] priorityList) {
        this.priorityList = Arrays.copyOf(priorityList, priorityList.length);
    }

    public void setIndividualTimeout(final int individualTimeout) {
        if (individualTimeout > 0) {
            this.individualTimeout = individualTimeout;
        }
    }

    public void setInternalReadingPeriod(final long internalReadingPeriod) {
        if (internalReadingPeriod > 0) {
            this.internalReadingPeriod = internalReadingPeriod;
        }
    }

    public void setDevice(final DeviceManager device) {
        this.device = device;
    }

}